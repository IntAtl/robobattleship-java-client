package ua.com.jbs.robobattleship.client.model;

import ua.com.jbs.robobattleship.client.rest.model.ShootResult;

import java.util.Deque;
import java.util.LinkedList;
import java.util.stream.IntStream;

/**
 * Created by Victor Novik - novikvictor@gmail.com.
 */
public class EnemyGrid {
    public static final int X_LENGTH = 10;
    public static final int Y_LENGTH = 10;
    public static final int HIT_SHIP_SIGH = 1;
    public static final int MISS_SIGH = -1;

    private int[][] grid = null;
    private Deque<ShootCoordinates> shootHistory;

    public void setShootCoordinate(
                    ShootCoordinates shootCoordinates,
                    ShootResult.ShootResultEnum shootResult) {
        switch (shootResult) {
            case HIT:
                getGrid()[shootCoordinates.x][shootCoordinates.y] = HIT_SHIP_SIGH;
                break;
            case TOUCHDOWN:
                getGrid()[shootCoordinates.x][shootCoordinates.y] = HIT_SHIP_SIGH;
                break;
            case MISS:
                getGrid()[shootCoordinates.x][shootCoordinates.y] = MISS_SIGH;
                break;
        }

        getShootHistory().add(shootCoordinates);
    }

    public int[][] getGrid() {
        if (this.grid == null) {
            this.grid = new int[X_LENGTH][Y_LENGTH];
            IntStream.range(0, X_LENGTH).forEach(
                            i -> IntStream.range(0, Y_LENGTH).forEach(j -> this.grid[i][j] = 0));
        }
        return grid;
    }

    public Deque<ShootCoordinates> getShootHistory() {
        if (this.shootHistory == null) {
            this.shootHistory = new LinkedList<>();
        }
        return shootHistory;
    }
}
