package ua.com.jbs.robobattleship.client.rest.validator;

import ua.com.jbs.robobattleship.client.rest.model.ResponseStatus;
import ua.com.jbs.robobattleship.client.rest.validator.GeneralResponseValidator.ResponseHandlerStatus;

/**
 * Created by Victor Novik - novikvictor@gmail.com.
 */
public interface ServerResponseValidator<T extends ResponseStatus> {
    ResponseHandlerStatus handle(T response);
}
