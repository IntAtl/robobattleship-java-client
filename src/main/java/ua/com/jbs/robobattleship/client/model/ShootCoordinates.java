package ua.com.jbs.robobattleship.client.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public class ShootCoordinates {
    public int x;
    public int y;

    public ShootCoordinates(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        ShootCoordinates that = (ShootCoordinates) o;

        return new EqualsBuilder()
                .append(x, that.x)
                .append(y, that.y)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(x)
                .append(y)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "ShootCoordinates{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
