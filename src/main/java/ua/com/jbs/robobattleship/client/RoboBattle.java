package ua.com.jbs.robobattleship.client;

import java.util.stream.IntStream;

import ua.com.jbs.robobattleship.client.model.*;
import ua.com.jbs.robobattleship.client.rest.model.Player;
import ua.com.jbs.robobattleship.client.rest.model.ShootResult;
import ua.com.jbs.robobattleship.client.rest.validator.ServerResponseValidator;
import ua.com.jbs.robobattleship.client.rest.validator.GeneralResponseValidator.ResponseHandlerStatus;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public class RoboBattle implements Battle, Logger {
    private ShootingCoordinateCalculator shootingDataCalculator;
    private PlayerService playerService;
    private ServerResponseValidator<ShootResult> serverResponseValidator;
    private EnemyGrid enemyGrid = new EnemyGrid();

    public RoboBattle(
                    ShootingCoordinateCalculator shootingDataCalculator,
                    PlayerService playerService,
                    ServerResponseValidator<ShootResult> serverResponseValidator) {
        this.shootingDataCalculator = shootingDataCalculator;
        this.playerService = playerService;
        this.serverResponseValidator = serverResponseValidator;
    }

    public void doBattle(Player player, String enemyUid) {
        getLogger().info("Start fighting between [{} - {}] and [{}]", player.uid, player.name,
                        enemyUid);

        ResponseHandlerStatus status = ResponseHandlerStatus.OK;
        ShootResult.ShootResultEnum shootStatus = ShootResult.ShootResultEnum.MISS;

        while (!status.equals(ResponseHandlerStatus.BATTLE_IS_OVER)) {
            ShootCoordinates shootCoordinates = shootingDataCalculator.nextCoordinate(shootStatus,
                            enemyGrid);

            ShootResult shootResult = playerService.shoot(player, enemyUid, shootCoordinates);
            status = serverResponseValidator.handle(shootResult);

            if (shootResult != null && shootResult.result != null) {
                enemyGrid.setShootCoordinate(shootCoordinates, shootResult.result);
            }
        }
    }
}
