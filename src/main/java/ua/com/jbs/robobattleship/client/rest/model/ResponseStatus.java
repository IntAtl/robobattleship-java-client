package ua.com.jbs.robobattleship.client.rest.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.google.gson.annotations.SerializedName;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public class ResponseStatus {
    public Status status;
    public Error error;

    public ResponseStatus() {}

    public ResponseStatus(
                    Status status) {
        this.status = status;
    }

    public ResponseStatus(
                    Status status,
                    Error error) {
        this.status = status;
        this.error = error;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("status", status)
                        .append("error", error).toString();
    }

    public static enum Status {
                               @SerializedName("success")
                               SUCCESS("success"),

                               @SerializedName("fail")
                               FAIL("fail");

        private String value;

        Status(
                        String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                            .append("value", value).toString();
        }
    }
}
