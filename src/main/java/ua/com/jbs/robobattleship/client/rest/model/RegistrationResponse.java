package ua.com.jbs.robobattleship.client.rest.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public class RegistrationResponse extends ResponseStatus {
    public Player player;

    public RegistrationResponse() {
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append(super.toString())
                .append("player", player)
                .toString();
    }
}
