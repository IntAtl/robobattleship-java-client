package ua.com.jbs.robobattleship.client;

import ua.com.jbs.robobattleship.client.model.EnemyGrid;
import ua.com.jbs.robobattleship.client.model.ShootCoordinates;
import ua.com.jbs.robobattleship.client.rest.model.ShootResult;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public interface ShootingCoordinateCalculator {
    /**
     * Calculates coordinate to shoot
     * @param status - Result of previous shooting
     * @param enemyShootingGrid - The grid of your shooting
     * @return Next coordinate to shoot
     */
    ShootCoordinates nextCoordinate(ShootResult.ShootResultEnum status, EnemyGrid enemyShootingGrid);
}
