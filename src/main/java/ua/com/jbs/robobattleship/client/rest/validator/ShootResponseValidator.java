package ua.com.jbs.robobattleship.client.rest.validator;

import ua.com.jbs.robobattleship.client.rest.model.ShootResult;

/**
 * Created by Victor Novik - novikvictor@gmail.com.
 */
public class ShootResponseValidator implements ServerResponseValidator<ShootResult> {
    private GeneralResponseValidator generalResponseValidator;

    public ShootResponseValidator(GeneralResponseValidator generalResponseValidator) {
        this.generalResponseValidator = generalResponseValidator;
    }

    @Override
    public GeneralResponseValidator.ResponseHandlerStatus handle(ShootResult response) {
        GeneralResponseValidator.ResponseHandlerStatus superHandleResult = generalResponseValidator.handle(response);

        if (GeneralResponseValidator.ResponseHandlerStatus.OK.equals(superHandleResult)
                && ShootResult.ShootResultEnum.WIN.equals(response.result)) {
            return GeneralResponseValidator.ResponseHandlerStatus.BATTLE_IS_OVER;
        }

        return superHandleResult;
    }
}
