package ua.com.jbs.robobattleship.client.model;

import ua.com.jbs.robobattleship.client.rest.model.Player;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public class BattleInfo {
    public Player player;
    public String enemyUid;
}
