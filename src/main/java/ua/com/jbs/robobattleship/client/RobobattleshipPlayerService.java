package ua.com.jbs.robobattleship.client;

import ua.com.jbs.robobattleship.client.model.ShootCoordinates;
import ua.com.jbs.robobattleship.client.rest.RobobattleshipRest;
import ua.com.jbs.robobattleship.client.rest.model.Player;
import ua.com.jbs.robobattleship.client.rest.model.RegistrationResponse;
import ua.com.jbs.robobattleship.client.rest.model.ResponseStatus;
import ua.com.jbs.robobattleship.client.rest.model.ShootResult;
import ua.com.jbs.robobattleship.client.rest.validator.GeneralResponseValidator;
import ua.com.jbs.robobattleship.client.rest.validator.ServerResponseValidator;

/**
 * Created by Victor Novik - novikvictor@gmail.com.
 */
public class RobobattleshipPlayerService implements PlayerService, Logger {
    private RobobattleshipRest client;
    private ServerResponseValidator<ResponseStatus> responseValidator;
    private ServerResponseValidator<ShootResult> shootResponseValidator;

    public RobobattleshipPlayerService(
                    RobobattleshipRest client,
                    ServerResponseValidator<ResponseStatus> responseValidator,
                    ServerResponseValidator<ShootResult> shootResponseValidator) {
        this.client = client;
        this.responseValidator = responseValidator;
        this.shootResponseValidator = shootResponseValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Player registerPlayer(String playerName) {
        getLogger().info("Registering player [{}]", playerName);

        RegistrationResponse response = this.client.register(playerName);

        getLogger().debug("Player registration response []", response);
        responseValidator.handle(response);

        getLogger().info("Player info [name=[{}], uid=[{}]]", response.player.name,
                        response.player.uid);
        return response.player;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendPlayerBattleshipMap(String playerUid, String playerSecret, String battleMap) {
        getLogger().info("Player {} battle ship map [{}]", playerUid, battleMap);

        ResponseStatus responseStatus = client.sendBattleMap(playerUid, playerSecret, battleMap);
        responseValidator.handle(responseStatus);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ShootResult shoot(Player player, String enemyUid, ShootCoordinates shootCoordinates) {
        GeneralResponseValidator.ResponseHandlerStatus status;
        getLogger().info("Shooting info [playerUid=[{}], enemyUid=[{}], x=[{}], y=[{}]]",
                        player.uid, enemyUid, shootCoordinates.x, shootCoordinates.y);
        ShootResult shootResult;
        do {
            shootResult = client.shoot(player.uid, player.secret, enemyUid,
                            Integer.toString(shootCoordinates.x),
                            Integer.toString(shootCoordinates.y));

            status = shootResponseValidator.handle(shootResult);

            getLogger().debug("Shooting status result {}", status);

        } while (isSuccessfulShootResult(status));

        getLogger().info("Shoot result is [{}]", shootResult.result);
        return shootResult;
    }

    private boolean isSuccessfulShootResult(GeneralResponseValidator.ResponseHandlerStatus status) {
        return !(GeneralResponseValidator.ResponseHandlerStatus.OK.equals(status)
                        || GeneralResponseValidator.ResponseHandlerStatus.BATTLE_IS_OVER
                                        .equals(status));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Player setUpPlayer(String playerName, String battleGrid) {
        Player player = registerPlayer(playerName);
        sendPlayerBattleshipMap(player.uid, player.secret, battleGrid);
        return player;
    }
}
