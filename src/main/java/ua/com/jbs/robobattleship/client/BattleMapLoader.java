package ua.com.jbs.robobattleship.client;

/**
 * Created by Victor Novik - novikvictor@gmail.com
 */
public interface BattleMapLoader {
    String load(String location);
}
