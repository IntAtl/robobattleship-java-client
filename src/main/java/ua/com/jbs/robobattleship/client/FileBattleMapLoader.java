package ua.com.jbs.robobattleship.client;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by Victor Novik - novikvictor@gmail.com
 */
public class FileBattleMapLoader implements BattleMapLoader, Logger {
    @Override
    public String load(String location) {
        try {
            // URL resource = getClass().getResource(location);
            // if (resource == null) {
            // throw new FileNotFoundException(String.format("File [%s] was not found", location));
            // }
            getLogger().info("loading file {}", location);
            // URI uri = new URI(location);
            try (PathReference pr = getPath(location)) {

                String joinedString = Files.readAllLines(pr.getPath()).stream()
                                .collect(Collectors.joining());

                validateGrid(joinedString);
                return joinedString;
            }
        } catch (Exception e) {
            throw new RuntimeException(
                            String.format("Can't load battle map from file [%s]", location), e);
        }
    }

    private void validateGrid(String grid) {
        if (grid == null || grid.chars().mapToObj(i -> (char) i)
                        .filter(ch -> ch == '1' || ch == '0').count() != 100) {
            throw new IllegalArgumentException(
                            "Battle grid should contains only 0 or 1 digits, and has to be 10x10");
        }

        if (grid.chars().mapToObj(i -> (char) i).filter(ch -> ch == '1').count() != 20) {
            throw new IllegalArgumentException("Wrong number of ships.");
        }

        if (grid.chars().mapToObj(i -> (char) i).filter(ch -> ch == '0').count() != 80) {
            throw new IllegalArgumentException("Wrong number of empty cells.");
        }
    }

    private PathReference getPath(String fileLocation) throws IOException, URISyntaxException {
        try {

            // first try getting a path via existing file systems
            ;
            URL resource = getClass().getResource(fileLocation);
            if (resource == null) {
                return new PathReference(Paths.get(fileLocation), null);
            }

            Path path = Paths.get(resource.toURI());
            getLogger().info("File loaded from FS");
            return new PathReference(path, null);
        } catch (final FileSystemNotFoundException e) {
            /*
             * not directly on file system, so then it's somewhere else (e.g.: JAR)
             */
            final Map<String, ?> env = Collections.emptyMap();
            URI uri = getClass().getResource(fileLocation).toURI();
            final FileSystem fs = FileSystems.newFileSystem(uri, env);

            getLogger().info("File loaded from JAR");
            return new PathReference(fs.provider().getPath(uri), fs);
        }
    }

    private static final class PathReference implements AutoCloseable {
        private final FileSystem fs;
        private final Path path;

        public PathReference(
                        Path path,
                        FileSystem fs) {
            this.fs = fs;
            this.path = path;
        }

        @Override
        public void close() throws Exception {
            if (fs != null) {
                fs.close();
            }
        }

        public FileSystem getFs() {
            return fs;
        }

        public Path getPath() {
            return path;
        }
    }
}
