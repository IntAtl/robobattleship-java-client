package ua.com.jbs.robobattleship.client.rest.validator;

import java.util.Objects;

import ua.com.jbs.robobattleship.client.rest.model.ResponseStatus;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public class GeneralResponseValidator
        implements ServerResponseValidator<ResponseStatus> {

    public ResponseHandlerStatus handle(ResponseStatus response) {
        Objects.requireNonNull(response, "Response can't be null");

        switch (response.status) {
            case SUCCESS: return ResponseHandlerStatus.OK;
            case FAIL: {
                return handleFailStatus(response);
            }
        }

        throw new IllegalStateException("It's not gonna be.");

    }

    private ResponseHandlerStatus handleFailStatus(ResponseStatus response) {
        switch (response.error.code) {
            case 301:
                return ResponseHandlerStatus.NOT_YOUR_TURN_TO_SHOOT;
            case 304:
                return ResponseHandlerStatus.BATTLE_IS_OVER;

            case 101:
            case 201:
            case 202:
            case 203:
            case 204:
            case 205:
            case 206:
            case 207:
            case 208:
            case 209:
            case 210:
            case 211:
            case 212:
            case 213:
            case 214:
            case 302:
            case 303:
            default:
                throw new RuntimeException(response.toString());
        }
    }

    public static enum ResponseHandlerStatus {
        OK, NOT_YOUR_TURN_TO_SHOOT, BATTLE_IS_OVER
    }
}
