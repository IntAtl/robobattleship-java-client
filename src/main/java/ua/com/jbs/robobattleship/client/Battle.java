package ua.com.jbs.robobattleship.client;

import ua.com.jbs.robobattleship.client.rest.model.Player;

/**
 * Created by Victor Novik - novikvictor@gmail.com.
 */
public interface Battle {
    void doBattle(Player player, String enemyUid);
}
