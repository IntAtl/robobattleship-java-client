package ua.com.jbs.robobattleship.client;

import ua.com.jbs.robobattleship.client.model.ShootCoordinates;
import ua.com.jbs.robobattleship.client.rest.model.Player;
import ua.com.jbs.robobattleship.client.rest.model.ShootResult;

/**
 * Created by Victor Novik - novikvictor@gmail.com.
 */
public interface PlayerService {
    /**
     * Registers player on server
     * @param playerName - Registration player name
     * @return Player's registration data such as uid and secret
     */
    Player registerPlayer(String playerName);

    /**
     * Sends player's battle ship map
     * @param playerUid - Player uid, the result of registration
     * @param playerSecret - Player secret, the result of registration
     * @param battleMap - Sequence of 0 and 1.
     */
    void sendPlayerBattleshipMap(String playerUid, String playerSecret, String battleMap);

    /**
     * Shoots the enemy
     * @param player - Player object with uid and secret
     * @param enemyUid - Enemy uid
     * @param shootCoordinates - Coordinates to shoot
     * @return Result of shooting the enemy
     */
    ShootResult shoot(Player player, String enemyUid, ShootCoordinates shootCoordinates);

    /**
     * Sets up player on server
     * @param playerName - Player name
     * @param battleGrid - Player battle grid
     * @return Registered player info
     */
    Player setUpPlayer(String playerName, String battleGrid);
}
