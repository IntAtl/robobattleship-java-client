package ua.com.jbs.robobattleship.client.rest.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public class Player {
    public String secret;
    public String name;
    public String uid;

    public Player() {
    }

    public Player(String name, String uid, String secret) {
        this.name = name;
        this.uid = uid;
        this.secret = secret;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("secret", secret)
                .append("name", name)
                .append("uid", uid)
                .toString();
    }
}
