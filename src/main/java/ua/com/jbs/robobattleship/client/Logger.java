package ua.com.jbs.robobattleship.client;

import org.slf4j.LoggerFactory;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public interface Logger {
    default org.slf4j.Logger getLogger() {
        return LoggerFactory.getLogger(getClass());
    }
}
