package ua.com.jbs.robobattleship.client.rest.model;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.google.gson.annotations.SerializedName;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public class ShootResult extends ResponseStatus {
    public ShootResultEnum result;

    public ShootResult() {}

    public ShootResult(
                    ShootResultEnum result,
                    ResponseStatus.Status status) {
        super(status);

        this.result = result;
    }

    public ShootResult(
                    ShootResultEnum result,
                    ResponseStatus.Status status,
                    Error error) {
        super(status, error);

        this.result = result;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append(super.toString())
                        .append("result", result).toString();
    }

    public static enum ShootResultEnum {
                                        @SerializedName("miss")
                                        MISS("miss"),
                                        @SerializedName("hit")
                                        HIT("hit"),
                                        @SerializedName("touchdown")
                                        TOUCHDOWN("touchdown"),
                                        @SerializedName("win")
                                        WIN("win");
        private final String value;

        ShootResultEnum(
                        String value) {
            this.value = value;
        }
    }
}
