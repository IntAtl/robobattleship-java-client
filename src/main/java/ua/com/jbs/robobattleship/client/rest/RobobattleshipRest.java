package ua.com.jbs.robobattleship.client.rest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;

import feign.Feign;
import feign.Logger;
import feign.Param;
import feign.RequestLine;
import feign.gson.GsonDecoder;
import feign.slf4j.Slf4jLogger;
import ua.com.jbs.robobattleship.client.rest.model.RegistrationResponse;
import ua.com.jbs.robobattleship.client.rest.model.ResponseStatus;
import ua.com.jbs.robobattleship.client.rest.model.ShootResult;

/**
 * @author Victor Novik - novikvictor@gmail.com
 */
public interface RobobattleshipRest {
    org.slf4j.Logger LOGGER = LoggerFactory.getLogger(RobobattleshipRest.class);
    
    @RequestLine("GET /register/{playerName}")
    RegistrationResponse register(@Param("playerName") String playerName);

    @RequestLine("GET /setships/{uid}/{secret}/{ships}")
    ResponseStatus sendBattleMap(
            @Param("uid") String uid,
            @Param("secret") String secret,
            @Param("ships") String ships);
    
    @RequestLine("GET /shoot/{uid}/{secret}/{enemy_uid}/{x}/{y}")
    ShootResult shoot(@Param("uid") String uid,
                      @Param("secret") String secret,
                      @Param("enemy_uid") String enemyUid,
                      @Param("x") String xCoordinate,
                      @Param("y") String yCoordinate);
    
    static RobobattleshipRest connect(String serverUrl) {
        if (StringUtils.isBlank(serverUrl)) {
            throw new IllegalArgumentException("Server URL can't be empty");
        }

        LOGGER.info("Init Robotattleship ua.com.jbs.robobattleship.ua.com.jbs.robobattleship.client with server URL [{}]", serverUrl);
        GsonDecoder decoder = new GsonDecoder();
        return Feign.builder()
                .decoder(decoder)
                .logger(new Slf4jLogger())
                .logLevel(Logger.Level.FULL)
                .target(RobobattleshipRest.class, serverUrl);
    }
}
