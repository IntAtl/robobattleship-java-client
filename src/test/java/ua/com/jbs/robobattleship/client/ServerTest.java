package ua.com.jbs.robobattleship.client;

import org.junit.Assert;

import ua.com.jbs.robobattleship.client.rest.RobobattleshipRest;
import ua.com.jbs.robobattleship.client.rest.model.Player;
import ua.com.jbs.robobattleship.client.rest.model.RegistrationResponse;
import ua.com.jbs.robobattleship.client.rest.model.ResponseStatus;
import ua.com.jbs.robobattleship.client.rest.model.ShootResult;

/**
 * Created by Victor Novik - novikvictor@gmail.com
 */
public class ServerTest {
//    @Test
    public void testBatleIsOver() {
        RobobattleshipRest client = RobobattleshipRest.connect("http://localhost:9999");
        FileBattleMapLoader battleMapLoader = new FileBattleMapLoader();

//        Set up first player
        String firstBattleMap = battleMapLoader.load("/single_ship_in_the_beginning.txt");
        Player firstPlayer = setUpPlayer(client, generateUniquePlayerName(), firstBattleMap);


//        Set up second player
        String secondBattleMap = battleMapLoader.load("/single_ship_in_the_end.txt");
        Player secondPlayer = setUpPlayer(client, generateUniquePlayerName(), secondBattleMap);

        shootAndMiss(client, secondPlayer, firstPlayer.uid, "0", "0");

        shootAndMiss(client, firstPlayer, secondPlayer.uid, "0", "0");

        ShootResult tShootResult = client.shoot(secondPlayer.uid, secondPlayer.secret, firstPlayer.uid, "0", "1");
        Assert.assertTrue(ResponseStatus.Status.SUCCESS.equals(tShootResult.status));
        Assert.assertTrue(ShootResult.ShootResultEnum.WIN.equals(tShootResult.result));

        ShootResult fshootResult = client.shoot(firstPlayer.uid, firstPlayer.secret, secondPlayer.uid, "9", "3");
        Assert.assertTrue(ResponseStatus.Status.FAIL.equals(fshootResult.status));
        Assert.assertTrue(fshootResult.error.code == 304);

        /*
            Really ?
        */
        ShootResult mShootResult = client.shoot(secondPlayer.uid, secondPlayer.secret, firstPlayer.uid, "0", "2");
        Assert.assertTrue(ResponseStatus.Status.SUCCESS.equals(mShootResult.status));
        Assert.assertTrue(ShootResult.ShootResultEnum.MISS.equals(mShootResult.result));

    }

    private void shootAndMiss(RobobattleshipRest client, Player player, String enemyUid, String x, String y) {
        ShootResult tShootResult = client.shoot(player.uid, player.secret, enemyUid, x, y);
//        Assert.assertTrue(ResponseStatus.Status.SUCCESS.equals(tShootResult.status));
//        Assert.assertTrue(ShootResult.ShootResultEnum.MISS.equals(tShootResult.result));
    }

    private String generateUniquePlayerName() {
        return "player" + Long.toString(System.currentTimeMillis());
    }

    private Player setUpPlayer(RobobattleshipRest client, String playerName, String playerBattleMap) {
        RegistrationResponse playerRegistrationResult = client.register(playerName);
        client.sendBattleMap(playerRegistrationResult.player.uid, playerRegistrationResult.player.secret, playerBattleMap);
        return playerRegistrationResult.player;
    }
}
