package ua.com.jbs.robobattleship.client;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import ua.com.jbs.robobattleship.client.model.ShootCoordinates;
import ua.com.jbs.robobattleship.client.rest.RobobattleshipRest;
import ua.com.jbs.robobattleship.client.rest.model.Error;
import ua.com.jbs.robobattleship.client.rest.model.Player;
import ua.com.jbs.robobattleship.client.rest.model.ResponseStatus;
import ua.com.jbs.robobattleship.client.rest.model.ShootResult;
import ua.com.jbs.robobattleship.client.rest.validator.GeneralResponseValidator;
import ua.com.jbs.robobattleship.client.rest.validator.ShootResponseValidator;

/**
 * Created by Victor Novik - novikvictor@gmail.com.
 */
public class ShootingLogicTest {

    @BeforeClass
    public static void setUp() {}

    @Test
    public void shootMissCase() {
        String playerName = "the-name";
        String playerUid = "the-name-uid";
        String playerSecret = "the-name-secret";
        String enemyUid = "enemy-uid";
        int x = 1;
        int y = 1;

        RobobattleshipRest client = Mockito.mock(RobobattleshipRest.class);
        ShootResult shootResult = new ShootResult(ShootResult.ShootResultEnum.MISS,
                        ResponseStatus.Status.SUCCESS);
        Mockito.when(client.shoot(playerUid, playerSecret, enemyUid, Integer.toString(x),
                        Integer.toString(y))).thenReturn(shootResult);

        Player player = new Player(playerName, playerUid, playerSecret);
        GeneralResponseValidator generalResponseValidator = new GeneralResponseValidator();
        ShootResponseValidator shootResponseValidator = new ShootResponseValidator(
                        generalResponseValidator);
        RobobattleshipPlayerService playerService = new RobobattleshipPlayerService(client,
                        generalResponseValidator, shootResponseValidator);
        ShootResult shootR = playerService.shoot(player, enemyUid, new ShootCoordinates(x, y));

        Mockito.verify(client, Mockito.only()).shoot(playerUid, playerSecret, enemyUid,
                        Integer.toString(x), Integer.toString(y));

        Assert.assertTrue(ShootResult.ShootResultEnum.MISS.equals(shootR.result));
    }

    @Test
    public void notYourTurnCase() {
        String playerName = "the-name";
        String playerUid = "the-name-uid";
        String playerSecret = "the-name-secret";
        String enemyUid = "enemy-uid";
        int x = 2;
        int y = 3;
        Error error = new Error(
                        "It's not your turn to shoot, wait until your opponent shoots and shoot again",
                        301);
        ShootResult notYourTurnResult = new ShootResult(null, ResponseStatus.Status.FAIL, error);
        ShootResult successResult = new ShootResult(ShootResult.ShootResultEnum.HIT,
                        ResponseStatus.Status.SUCCESS);

        RobobattleshipRest client = Mockito.mock(RobobattleshipRest.class);
        Mockito.when(client.shoot(playerUid, playerSecret, enemyUid, Integer.toString(x),
                        Integer.toString(y))).thenReturn(notYourTurnResult, successResult);

        GeneralResponseValidator generalResponseValidator = new GeneralResponseValidator();
        ShootResponseValidator shootResponseValidator = new ShootResponseValidator(
                        generalResponseValidator);
        RobobattleshipPlayerService playerService = new RobobattleshipPlayerService(client,
                        generalResponseValidator, shootResponseValidator);
        ShootResult resultEnum = playerService.shoot(
                        new Player(playerName, playerUid, playerSecret), enemyUid,
                        new ShootCoordinates(x, y));

        Mockito.verify(client, Mockito.times(2)).shoot(playerUid, playerSecret, enemyUid,
                        Integer.toString(x), Integer.toString(y));

        Assert.assertTrue(ShootResult.ShootResultEnum.HIT.equals(resultEnum.result));
    }

    @Test
    public void battleIsOverCase() {
        String playerName = "the-name";
        String playerUid = "the-name-uid";
        String playerSecret = "the-name-secret";
        String enemyUid = "enemy-uid";
        int x = 1;
        int y = 6;

        ShootResult battleIsOverResult = new ShootResult(null, ResponseStatus.Status.FAIL,
                        new Error("This battle is over. Winner - . Looser - .", 304));

        RobobattleshipRest client = Mockito.mock(RobobattleshipRest.class);
        Mockito.when(client.shoot(playerUid, playerSecret, enemyUid, Integer.toString(x),
                        Integer.toString(y))).thenReturn(battleIsOverResult);

        GeneralResponseValidator generalResponseValidator = new GeneralResponseValidator();
        ShootResponseValidator shootResponseValidator = new ShootResponseValidator(
                        generalResponseValidator);
        RobobattleshipPlayerService playerService = new RobobattleshipPlayerService(client,
                        generalResponseValidator, shootResponseValidator);

        ShootResult resultEnum = playerService.shoot(
                        new Player(playerName, playerUid, playerSecret), enemyUid,
                        new ShootCoordinates(x, y));

        Mockito.verify(client, Mockito.only()).shoot(playerUid, playerSecret, enemyUid,
                        Integer.toString(x), Integer.toString(y));

        Assert.assertTrue(resultEnum.error.code == 304);
    }
}
